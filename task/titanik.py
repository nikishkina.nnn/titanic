import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    
    title_group = titanic_df[titanic_df['Name'].str.contains(title)]
        
        # Calculate the median age for this group
        median_age = title_group['Age'].median()
        
        # Count the number of missing values for this group
        missing_values = title_group['Age'].isnull().sum()
        
        # Fill missing values with the calculated median age
        title_group['Age'].fillna(median_age, inplace=True)
        
        # Update the results list
        results.append((title, missing_values, round(median_age)))
    
    return results
